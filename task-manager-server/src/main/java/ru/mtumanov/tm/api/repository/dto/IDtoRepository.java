package ru.mtumanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.AbstractModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface IDtoRepository<M extends AbstractModelDTO> {

    void add(@NotNull M entity) throws AbstractException;

    @NotNull
    M update(@NotNull M entity) throws AbstractException;

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    long getSize();

    void removeById(@NotNull String id);

    boolean existById(@NotNull String id);

    @NotNull
    EntityManager getEntityManager();

}
