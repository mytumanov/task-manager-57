package ru.mtumanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.api.repository.dto.IDtoUserRepository;
import ru.mtumanov.tm.dto.model.UserDTO;

import javax.persistence.NoResultException;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IDtoUserRepository {

    @Override
    public void clear() {
        getEntityManager().createQuery("DELETE FROM UserDTO")
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<UserDTO> findAll() {
        return getEntityManager().createQuery("FROM UserDTO p", UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<UserDTO> findAll(@NotNull final Comparator<UserDTO> comparator) {
        return getEntityManager().createQuery("FROM UserDTO p ORDER BY p." + getComporator(comparator), UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    @Nullable
    public UserDTO findOneById(@NotNull final String id) {
        return getEntityManager().find(UserDTO.class, id);
    }

    @Override
    public long getSize() {
        return getEntityManager().createQuery("SELECT COUNT(1) FROM UserDTO p", Long.class)
                .setHint("org.hibernate.cacheable", true)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        getEntityManager().createQuery("DELETE FROM UserDTO WHERE id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existById(@NotNull final String id) {
        try {
            findOneById(id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public @NotNull UserDTO findByLogin(@NotNull final String login) {
        return getEntityManager().createQuery("FROM UserDTO p WHERE login = :login", UserDTO.class)
                .setParameter("login", login)
                .setHint("org.hibernate.cacheable", true)
                .getSingleResult();
    }

    @Override
    public @NotNull UserDTO findByEmail(@NotNull final String email) {
        return getEntityManager().createQuery("FROM UserDTO p WHERE email = :email", UserDTO.class)
                .setParameter("email", email)
                .setHint("org.hibernate.cacheable", true)
                .getSingleResult();
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        try {
            findByLogin(login);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        try {
            findByEmail(email);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

}
