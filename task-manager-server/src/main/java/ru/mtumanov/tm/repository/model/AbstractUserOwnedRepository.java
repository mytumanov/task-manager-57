package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.model.IUserOwnedRepository;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void add(@NotNull final M entity) throws AbstractException {
        if (entity.getUser() == null)
            throw new UserIdEmptyException();
        super.add(entity);
    }

    @Override
    public @NotNull M update(@NotNull final M entity) throws AbstractException {
        if (entity.getUser() == null)
            throw new UserIdEmptyException();
        return super.update(entity);
    }

}
