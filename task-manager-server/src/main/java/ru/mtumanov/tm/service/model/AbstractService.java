package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.model.IRepository;
import ru.mtumanov.tm.api.service.model.IService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected EntityManager getEntityManager() {
        return getRepository().getEntityManager();
    }

    @NotNull
    protected abstract R getRepository();

    @Override
    public @NotNull List<M> findAll() {
        return getRepository().findAll();
    }

    @Override
    @NotNull
    public void set(@NotNull Collection<M> models) throws AbstractException {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (M model : models) {
                getRepository().add(model);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
