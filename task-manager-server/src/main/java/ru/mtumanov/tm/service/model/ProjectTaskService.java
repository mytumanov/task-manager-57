package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.mtumanov.tm.api.repository.model.IProjectRepository;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.api.service.model.IProjectTaskService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;

import javax.persistence.EntityManager;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    protected EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    protected ITaskRepository getRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @NotNull
    protected IProjectRepository getProjectRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Override
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!getProjectRepository().existById(userId, projectId))
            throw new ProjectNotFoundException();


        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final Project project = getProjectRepository().findOneById(userId, projectId);
        @NotNull final Task task = repository.findOneById(userId, taskId);
        task.setProject(project);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (!getProjectRepository().existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final Project project = getProjectRepository().findOneById(userId, projectId);
        @NotNull final IProjectRepository repository = getProjectRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!getProjectRepository().existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final Task task = repository.findOneById(userId, taskId);
        task.setProject(null);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
