package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.mtumanov.tm.api.repository.model.IProjectRepository;
import ru.mtumanov.tm.api.service.model.IProjectService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.field.StatusNotSupportedException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.User;

import javax.persistence.EntityManager;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    @NotNull
    protected IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Override
    @NotNull
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final User user = getUserRepository().findOneById(userId);
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(user);
        try {
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @NotNull
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final Project project = repository.findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @NotNull
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (status == null)
            throw new StatusNotSupportedException();

        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final Project project = repository.findOneById(userId, id);
        project.setStatus(status);
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}
