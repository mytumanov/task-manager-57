package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.mtumanov.tm.api.repository.model.IUserOwnedRepository;
import ru.mtumanov.tm.api.repository.model.IUserRepository;
import ru.mtumanov.tm.api.service.model.IUserOwnedService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;
import ru.mtumanov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Override
    @NotNull
    protected abstract R getRepository();

    @NotNull
    protected IUserRepository getUserRepository() {
        return userRepository;
    }

    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) throws AbstractException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        @NotNull final User user = getUserRepository().findOneById(userId);
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            model.setUser(user);
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return getRepository().existById(userId, id);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        return getRepository().findAll(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (comparator == null)
            return getRepository().findAll(userId);
        return getRepository().findAll(userId, comparator);
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return getRepository().findOneById(userId, id);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return getRepository().getSize(userId);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
