package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.dto.IDtoUserOwnedRepository;
import ru.mtumanov.tm.api.service.dto.IDtoUserOwnedService;
import ru.mtumanov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDtoUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IDtoUserOwnedRepository<M>>
        extends AbstractDtoService<M, R> implements IDtoUserOwnedService<M> {

    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) throws AbstractException {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        model.setUserId(userId);
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return getRepository().existById(userId, id);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        return getRepository().findAll(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (comparator == null)
            return getRepository().findAll(userId);
        return getRepository().findAll(userId, comparator);
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return getRepository().findOneById(userId, id);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return getRepository().getSize(userId);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeById(userId, model.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
