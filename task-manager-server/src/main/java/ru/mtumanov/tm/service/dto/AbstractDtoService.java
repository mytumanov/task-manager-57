package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.dto.IDtoRepository;
import ru.mtumanov.tm.api.service.dto.IDtoService;
import ru.mtumanov.tm.dto.model.AbstractModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    protected abstract R getRepository();

    @NotNull
    protected EntityManager getEntityManager() {
        return getRepository().getEntityManager();
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return getRepository().findAll();
    }

    @Override
    public void set(@NotNull final Collection<M> models) throws AbstractException {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (M model : models) {
                repository.add(model);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
