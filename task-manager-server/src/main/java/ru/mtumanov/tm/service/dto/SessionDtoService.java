package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.mtumanov.tm.api.repository.dto.IDtoSessionRepository;
import ru.mtumanov.tm.api.service.dto.IDtoSessionService;
import ru.mtumanov.tm.dto.model.SessionDTO;

@Service
public class SessionDtoService extends AbstractDtoUserOwnedService<SessionDTO, IDtoSessionRepository> implements IDtoSessionService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    @NotNull
    protected IDtoSessionRepository getRepository() {
        return context.getBean(IDtoSessionRepository.class);
    }

}
