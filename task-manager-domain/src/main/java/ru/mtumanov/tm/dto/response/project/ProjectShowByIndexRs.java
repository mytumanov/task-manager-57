package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectShowByIndexRs extends AbstractProjectRs {

    public ProjectShowByIndexRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectShowByIndexRs(@Nullable final Throwable err) {
        super(err);
    }

}
