package ru.mtumanov.tm.dto.response.server;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractRs;

@Getter
@Setter
public class ServerVersionRs extends AbstractRs {

    @Nullable
    private String version;

}
