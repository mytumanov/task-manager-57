package ru.mtumanov.tm.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends Exception {

    protected AbstractException() {
    }

    protected AbstractException(@NotNull final String message) {
        super(message);
    }

    protected AbstractException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    protected AbstractException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
