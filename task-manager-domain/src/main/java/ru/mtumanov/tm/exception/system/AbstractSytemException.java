package ru.mtumanov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

public abstract class AbstractSytemException extends AbstractException {

    protected AbstractSytemException() {
    }

    protected AbstractSytemException(@NotNull final String message) {
        super(message);
    }

    protected AbstractSytemException(@NotNull final String messgae, @NotNull final Throwable cause) {
        super(messgae, cause);
    }

    protected AbstractSytemException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractSytemException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
