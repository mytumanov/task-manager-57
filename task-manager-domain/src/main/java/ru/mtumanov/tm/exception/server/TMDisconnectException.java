package ru.mtumanov.tm.exception.server;

import org.jetbrains.annotations.NotNull;

public class TMDisconnectException extends AbstractServerException {

    public TMDisconnectException() {
        super("ERROR! Can't connect to server!");
    }

    public TMDisconnectException(@NotNull final String message) {
        super(message);
    }

}
