package ru.mtumanov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.*;
import ru.mtumanov.tm.dto.response.user.*;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, IUserEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(final String host, final String port) {
        return IEndpoint.newInstance(host, port, NAME, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserChangePasswordRs userChangePassword(@NotNull UserChangePasswordRq request);

    @NotNull
    @WebMethod
    UserLockRs userLock(@NotNull UserLockRq request);

    @NotNull
    @WebMethod
    UserRegistryRs userRegistry(@NotNull UserRegistryRq request);

    @NotNull
    @WebMethod
    UserRemoveRs userRemove(@NotNull UserRemoveRq request);

    @NotNull
    @WebMethod
    UserUnlockRs userUnlock(@NotNull UserUnlockRq request);

    @NotNull
    @WebMethod
    UserProfileRs userProfile(@NotNull UserProfileRq request);

    @NotNull
    @WebMethod
    UserUpdateRs userUpdate(@NotNull UserUpdateRq request);

}
