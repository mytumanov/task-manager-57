package ru.mtumanov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractModel implements Serializable {

    @Id
    @NotNull
    @Column(nullable = false)
    protected String id = UUID.randomUUID().toString();

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AbstractModel)) {
            return false;
        }
        AbstractModel abstractModel = (AbstractModel) o;
        return Objects.equals(id, abstractModel.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

}
