package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.request.user.UserLogoutRq;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.dto.response.user.UserLogoutRs;
import ru.mtumanov.tm.marker.SoapCategory;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final String host = "localhost";

    @NotNull
    private final String port = "6060";

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final String login = "NOT_COOL_USER";

    @NotNull
    private final String password = "Not Cool";

    @Test
    public void testErrLogin() {
        @NotNull final UserLoginRq request = new UserLoginRq("RANDOM USER", "RANDOM PASSWORD");
        @NotNull final UserLoginRs response = authEndpoint.login(request);
        assertNull(response.getToken());
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testLogin() {
        @NotNull final UserLoginRq request = new UserLoginRq(login, password);
        @NotNull final UserLoginRs response = authEndpoint.login(request);
        assertNotNull(response.getToken());
        assertTrue(response.getSuccess());
    }

    @Test
    public void testLogout() {
        @NotNull final UserLoginRq request = new UserLoginRq(login, password);
        @NotNull final UserLoginRs response = authEndpoint.login(request);
        assertNotNull(response.getToken());

        UserLogoutRs logOutResponse = authEndpoint.logout(new UserLogoutRq(response.getToken()));
        assertTrue(logOutResponse.getSuccess());
    }
}
