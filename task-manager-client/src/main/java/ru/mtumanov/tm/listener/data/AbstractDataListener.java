package ru.mtumanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.endpoint.IDomainEndpoint;
import ru.mtumanov.tm.listener.AbstractListener;

@Component
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

}
