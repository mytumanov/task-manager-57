package ru.mtumanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.data.DataBinaryLoadRq;
import ru.mtumanov.tm.dto.response.data.DataBinaryLoadRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class DataBinaryLoadListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-load-bin";

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from binary file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataBinaryLoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final DataBinaryLoadRs response = getDomainEndpoint().loadDataBinary(new DataBinaryLoadRq(getToken()));
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
