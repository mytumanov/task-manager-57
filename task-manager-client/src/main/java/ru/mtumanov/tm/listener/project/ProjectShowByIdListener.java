package ru.mtumanov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectShowByIdRq;
import ru.mtumanov.tm.dto.response.project.ProjectShowByIdRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class ProjectShowByIdListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Show project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRq request = new ProjectShowByIdRq(getToken(), id);
        @NotNull final ProjectShowByIdRs response = getProjectEndpoint().projectShowById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
        showProject(response.getProject());
    }

}
