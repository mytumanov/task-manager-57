package ru.mtumanov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.user.UserRemoveRq;
import ru.mtumanov.tm.dto.response.user.UserRemoveRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class UserRemoveListener extends AbstractUserListener {

    @Override
    @NotNull
    public String getDescription() {
        return "user remove";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-remove";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@userRemoveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRq request = new UserRemoveRq(getToken(), login);
        @NotNull final UserRemoveRs response = getUserEndpoint().userRemove(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
