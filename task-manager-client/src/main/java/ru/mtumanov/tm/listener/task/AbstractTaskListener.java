package ru.mtumanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.endpoint.IProjectEndpoint;
import ru.mtumanov.tm.api.endpoint.ITaskEndpoint;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.listener.AbstractListener;

import java.util.List;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    protected IProjectEndpoint getProjectEndpoint() {
        return serviceLocator.getProjectEndpoint();
    }

    @NotNull
    protected ITaskEndpoint getTaskEndpoint() {
        return serviceLocator.getTaskEndpoint();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void renderTask(final List<TaskDTO> tasks) {
        int index = 1;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus());
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

}
