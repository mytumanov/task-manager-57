package ru.mtumanov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.user.UserRegistryRq;
import ru.mtumanov.tm.dto.response.user.UserRegistryRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class UserRegistryListener extends AbstractUserListener {

    @Override
    @NotNull
    public String getDescription() {
        return "registry user";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-registry";
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRq request = new UserRegistryRq(getToken(), login, password, email);
        @NotNull final UserRegistryRs response = getUserEndpoint().userRegistry(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        showUser(response.getUser());
    }

}
