package ru.mtumanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskClearRq;
import ru.mtumanov.tm.dto.response.task.TaskClearRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class TaskClearListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-clear";
    }

    @Override
    @EventListener(condition = "@taskClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[TASK CLEAR]");
        @NotNull final TaskClearRq request = new TaskClearRq(getToken());
        @NotNull final TaskClearRs response = getTaskEndpoint().taskClear(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
