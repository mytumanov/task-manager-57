package ru.mtumanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.listener.AbstractListener;

@Component
public class ApplicationHelpListener extends AbstractSystemListener {

    @Override
    @NotNull
    public String getArgument() {
        return "-h";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show list arguments";
    }

    @Override
    @NotNull
    public String getName() {
        return "help";
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[HELP]");
        for (final AbstractListener abstractListener : listeners) System.out.println(abstractListener.toString());
    }

}
