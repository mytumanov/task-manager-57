package ru.mtumanov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.endpoint.*;
import ru.mtumanov.tm.api.service.ILoggerService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.api.service.ITokenService;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.listener.AbstractListener;
import ru.mtumanov.tm.util.SystemUtil;
import ru.mtumanov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Getter
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Getter
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Getter
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Getter
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Getter
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Getter
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(fileName), pid.getBytes());
        } catch (@NotNull final IOException e) {
            loggerService.error(e);
        }
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        init();

        while (Thread.currentThread().isAlive()) {
            System.out.println("ENTER COMMAND:");
            try {
                @NotNull final String cmd = TerminalUtil.nextLine();
                publisher.publishEvent(new ConsoleEvent(cmd));
                System.out.println("OK");
                loggerService.command(cmd);
            } catch (final Exception e) {
                System.out.println("FAIL");
                loggerService.error(e);
            }
        }
    }

    private void init() {
        loggerService.info("** WELCOME TO TASK_MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        initPID();
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}
